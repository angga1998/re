<?php
/**
 * Konfigurasi dasar WordPress.
 *
 * Berkas ini berisi konfigurasi-konfigurasi berikut: Pengaturan MySQL, Awalan Tabel,
 * Kunci Rahasia, Bahasa WordPress, dan ABSPATH. Anda dapat menemukan informasi lebih
 * lanjut dengan mengunjungi Halaman Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Menyunting wp-config.php}. Anda dapat memperoleh pengaturan MySQL dari web host Anda.
 *
 * Berkas ini digunakan oleh skrip penciptaan wp-config.php selama proses instalasi.
 * Anda tidak perlu menggunakan situs web, Anda dapat langsung menyalin berkas ini ke
 * "wp-config.php" dan mengisi nilai-nilainya.
 *
 * @package WordPress
 */

// ** Pengaturan MySQL - Anda dapat memperoleh informasi ini dari web host Anda ** //
/** Nama basis data untuk WordPress */
define( 'DB_NAME', 'profil_re' );

/** Nama pengguna basis data MySQL */
define( 'DB_USER', 'root' );

/** Kata sandi basis data MySQL */
define( 'DB_PASSWORD', '' );

/** Nama host MySQL */
define( 'DB_HOST', 'localhost' );

/** Set Karakter Basis Data yang digunakan untuk menciptakan tabel basis data. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Jenis Collate Basis Data. Jangan ubah ini jika ragu. */
define('DB_COLLATE', '');

/**#@+
 * Kunci Otentifikasi Unik dan Garam.
 *
 * Ubah baris berikut menjadi frase unik!
 * Anda dapat menciptakan frase-frase ini menggunakan {@link https://api.wordpress.org/secret-key/1.1/salt/ Layanan kunci-rahasia WordPress.org}
 * Anda dapat mengubah baris-baris berikut kapanpun untuk mencabut validasi seluruh cookies. Hal ini akan memaksa seluruh pengguna untuk masuk log ulang.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Y(Zn [hdtgGk4|&c/Pnf#WC)0>*N-{}x:DO(6VU$ZcOP&9b_:i?^3).v_`/{CH{q' );
define( 'SECURE_AUTH_KEY',  'CopRn!7=Kx3-&GaWl]B]~zA32_#I&uc>8G<6JXT#HWZcGMl,-dUrjTK~y+S!f+;h' );
define( 'LOGGED_IN_KEY',    '~/4CD8USX*qH;NP^?r}lZFUn`fl ^ <(DaC16APRvT*^%^8-zgfFpSNcE/#I7E4e' );
define( 'NONCE_KEY',        '^8}!Mu9XDq)mIf^r8$8S_b&X /pfe1 0Y&r%aUCOdo^~/^|*q3BH1^f(SFda/Nm#' );
define( 'AUTH_SALT',        'y,o3c(UP8 X1N$@q,,Y9wL[HDG8B-R/C0>OHe#H`4~>}+cOsG+bi&s9oHl!5BF0k' );
define( 'SECURE_AUTH_SALT', 'y4a.I+b>J~TI!z[q.4NRyq(]|n)R6?=|F<X7lT{i;uULyd*]W`+R%{6/>_DMA6}.' );
define( 'LOGGED_IN_SALT',   '}d/|)F1wx>m1$#BduG;oVP[07-EAu4GGa5#&4qgaQ~;C(eDlk,,Z6^<5R(rs&J:t' );
define( 'NONCE_SALT',       'G6a_k+Y%)Xed+Lx.x}tj/%;)7X]UK(.rxN,Y:%>XSrv4@aVn)/EPS9[_wI<>k5Hc' );

/**#@-*/

/**
 * Awalan Tabel Basis Data WordPress.
 *
 * Anda dapat memiliki beberapa instalasi di dalam satu basis data jika Anda memberikan awalan unik
 * kepada masing-masing tabel. Harap hanya masukkan angka, huruf, dan garis bawah!
 */
$table_prefix = 'wp_';

/**
 * Untuk pengembang: Moda pengawakutuan WordPress.
 *
 * Ubah ini menjadi "true" untuk mengaktifkan tampilan peringatan selama pengembangan.
 * Sangat disarankan agar pengembang plugin dan tema menggunakan WP_DEBUG
 * di lingkungan pengembangan mereka.
 */
define('WP_DEBUG', false);

/* Cukup, berhenti menyunting! Selamat ngeblog. */

/** Lokasi absolut direktori WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Menentukan variabel-variabel WordPress berkas-berkas yang disertakan. */
require_once(ABSPATH . 'wp-settings.php');
